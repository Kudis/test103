<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 09.07.19
 */

/**
 * Для удаления файлов, если это обычный магазин без каких-либо странных модулей:
 * 1. Ищем все файлы, лежащие в директории '/upload/iblock';
 * 2. Ищем все файлы зарегистрированные в системе для модуля 'iblock';
 * 3. Сравниваем массивы;
 * 4. Удаляем файлы, которые не зарегистрированы в модуле 'iblock', но лежат в директории '/upload/iblock';
 * Можно реализовать прямым обращением к базе, получив пути зарегистрированных файлов из таблицы 'b_file'
 */

function findFilesFromDirectory($path, &$files)
{
    if (is_dir($path)) {
        $cleanPath = array_diff(scandir($path), array('.', '..'));
        foreach ($cleanPath as $file) {
            $finalPath = $path . '/' . $file;
            $result = findFilesFromDirectory($finalPath, $files);
            if (!is_null($result)) $files[] = $result;
        }
    } else if (is_file($path)) {
        return $path;
    }
}

$docRoot = $_SERVER['DOCUMENT_ROOT'];
$dirPath = $docRoot . '/upload/iblock';
$arDiskFiles = [];
findFilesFromDirectory($dirPath, $arDiskFiles);

$arIblockFiles = [];
$obFiles = CFile::GetList(
    [],
    [
        'MODULE' => 'iblock',
    ]
);

while ($arFile = $obFiles->Fetch()) {
    $arIblockFiles[] = $docRoot . '/upload/' . $arFile['SUBDIR'] . '/' . $arFile['FILE_NAME'];
}

$arFilesToDelete = array_diff($arDiskFiles, $arIblockFiles);

foreach ($arFilesToDelete as $filePath) {
    unlink($filePath);
}


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

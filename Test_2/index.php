<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use Bitrix\Main\Loader,
    Bitrix\Main\LoaderException;

class MyBxGood {

    private $catalogIblockId;
    private $offerIblockId;
    private $id;
    public $LAST_ERROR = '';

    public function __construct($catalogIblockId, $offerIblockId)
    {
        $this->catalogIblockId = $catalogIblockId;
        $this->offerIblockId = $offerIblockId;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function addProduct($arFields)
    {
        
        if (!isset($arFields['NAME']) || empty($arFields['NAME'])) {
            return false;
        }
        
        try {
            if (!Loader::includeModule('iblock')) {
                return false;
            }
        } catch (LoaderException $e) {
            return false;
        }

        $arFields['IBLOCK_ID'] = $this->catalogIblockId;

        // TODO: Получить параметры транслитерации из настроек инфоблока
        $arFields['CODE'] = Cutil::translit($arFields['NAME'],LANGUAGE_ID);
        // TODO: Вставить проверку на уникальность CODE

        $el = new CIBlockElement;
        $productId = $el->Add($arFields);

        if ($productId) {
            $this->id = $productId;
            return true;
        } else {
            $this->LAST_ERROR = $el->LAST_ERROR;
            return false;
        }
    }

    public function addOffer($arFields)
    {
        if (!isset($arFields['NAME']) || empty($arFields['NAME'])) {
            return false;
        }

        try {
            if (
                !Loader::includeModule('iblock') ||
                !Loader::includeModule('sale')
            ) {
                return false;
            }
        } catch (LoaderException $e) {
            return false;
        }

        $arFields['IBLOCK_ID'] = $this->offerIblockId;
        $arFields['PROPERTY_VALUES']['CML2_LINK'] = $this->id;
        if (isset($arFields['QUANTITY']) && $arFields['QUANTITY'] > 0) {
            $offerQuantity = intval($arFields['QUANTITY']);
        } else {
            $offerQuantity = 0;
        }
        unset($arFields['QUANTITY']);

        if (isset($arFields['PRICES']) && is_array($arFields['PRICES']) && !empty($arFields['PRICES'])) {
            $arOfferPrices = $arFields['PRICES'];
        } else {
            $arOfferPrices = [];
        }
        unset($arFields['PRICES']);
        
        $el = new CIBlockElement;
        $offerId = $el->Add($arFields);
        
        if ($offerId > 0) {

            CCatalogProduct::Add(
                array(
                    'ID' => $offerId,
                    'QUANTITY' => $offerQuantity
                )
            );

            if (!empty($arOfferPrices)) {
                foreach ($arOfferPrices as $arOfferPrice) {
                    if (
                        isset($arOfferPrice['CURRENCY']) && !empty($arOfferPrice['CURRENCY']) &&
                        isset($arOfferPrice['PRICE']) && $arOfferPrice['PRICE'] > 0
                    ) {
                        $arOfferPrice['PRODUCT_ID'] = $offerId;
                    }
                    CPrice::Add($arOfferPrice);
                }
            }

        } else {
            $this->LAST_ERROR = $el->LAST_ERROR;
            return false;
        }
    }
}

$newProduct = new MyBxGood(14, 26);

if ($newProduct->addProduct([
    'NAME' => 'Новый товар',
    'ACTIVE' => 'Y',
    'IBLOCK_SECTION' => 62
])) {
    $arOfferFields = [
        'NAME' => 'Offer1',
        'ACTIVE' => 'Y',
        'QUANTITY' => 10,
        'PRICES' => [
            'CURRENCY' => 'RUB',
            'PRICE' => 10,
            'CATALOG_GROUP_ID' => 1
        ]
    ];

    $newProduct->addOffer($arOfferFields);
} else {
    echo $newProduct->LAST_ERROR;
}

?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
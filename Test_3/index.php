<?php
/**
 * @author Sergei Kudisov <programming@kudis.ru>
 * Date: 09.07.19
 */

function readTheFile($strPath)
{
    if (file_exists($strPath)) {
        if ($fd = fopen($strPath, 'r')) {

            while(!feof($fd)) {
                yield trim(fgets($fd));
            }
            fclose($fd);
        }
    }

    return '';
}

$strInPath = __DIR__ . '/chart.json';
$strOutPath = __DIR__ . '/chart_result.json';
$obPropertyValue = readTheFile($strInPath);

if (!empty($obPropertyValue)) {

    $strPropertyValue = '';
    foreach ($obPropertyValue as $string) {
        $strPropertyValue .= $string;
    }

    $arValue = json_decode($strPropertyValue, true);

    if (!empty($arValue)) {

        foreach ($arValue as $key => $arItem) {

            $length = count($arItem);

            if ($length > 4) { // при этом 100 может повторяться не более трех раз (4 = 3 значения и метка времени)

                $hundredPositions = [];

                for ($j = 1; $j < $length; $j++) {
                    $value = $arItem[$j];

                    if ($value == 100) {
                        $hundredPositions[] = $j;
                    }

                    if ($value != 100 || $j == $length - 1) {
                        if (count($hundredPositions) > 3) {
                            foreach ($hundredPositions as $hundredPosition) {
                                $arValue[$key][$hundredPosition] = null;
                            }
                        }
                        $hundredPositions = [];
                    }
                }
            }
        }

        $fp = fopen ($strOutPath, 'w');
        fwrite($fp,json_encode($arValue));
        fclose($fp);
    }
}
